package com.projetoempresas.lucas.projetoempresas.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas on 06/01/2018.
 */

public class UserResponse {

    // Atributos
     @SerializedName("status")
     private boolean resposta;
     @SerializedName("enterprise")
     private String enterprise;

        // Construtor padrão
        public UserResponse(boolean x, String enterprise) {
            this.enterprise = enterprise;
            this.resposta = x;
        }

        // Métodos getters e setters
        public void setEnterprise(String enterprise){
            this.enterprise = enterprise;
        }

        public String getEnterprise(){
            return this.enterprise;
        }

        public void setSuccess(boolean x){
            this.resposta = x;
        }

        // isSuccess = getSuccess
        public boolean isSuccess(){
            return this.resposta;
        }
}
