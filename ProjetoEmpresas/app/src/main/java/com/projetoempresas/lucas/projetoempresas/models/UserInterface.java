package com.projetoempresas.lucas.projetoempresas.models;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by lucas on 04/01/2018.
 */

public interface UserInterface {
    String ARQUIVO = "arquivo";
    String URL = "http://54.94.179.135:8090/api/v1/";


    @POST("users/auth/sign_in")
    Call<UserResponse> login(@Body User user);

}